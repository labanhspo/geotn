namespace GEOTN.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changepagekey : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Pages");
            AddPrimaryKey("dbo.Pages", "Alias");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Pages");
            AddPrimaryKey("dbo.Pages", "ID");
        }
    }
}
