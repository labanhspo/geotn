namespace GEOTN.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changepagekey2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Pages", "ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Pages", "ID", c => c.Int(nullable: false, identity: true));
        }
    }
}
