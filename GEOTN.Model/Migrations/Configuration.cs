﻿namespace GEOTN.Model.Migrations
{
    using Models;
    using System.Data.Entity.Migrations;
    using System.Text.RegularExpressions;    

    internal sealed class Configuration : DbMigrationsConfiguration<GEOTNDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(GEOTNDbContext context)
        {
            string mission = " Trường Đại học Công nghệ Thông tin và Truyền thông - ĐHTN là trung tâm đào tạo nguồn nhân lực có trình độ đại học, sau đại học; nghiên cứu khoa học và chuyển giao công nghệ thuộc lĩnh vực công nghệ thông tin và truyền thông phục vụ sự nghiệp công nghiệp hóa, hiện đại hóa đất nước";
            string vision = "Phấn đấu đến năm 2020, Trường Đại học Công nghệ Thông tin và Truyền thông sẽ trở thành trường đại học điện tử đạt đẳng cấp quốc gia, là một trong những Trung tâm hàng đầu của Việt Nam về đào tạo trình độ đại học, sau đại học, nghiên cứu khoa học và chuyển giao công nghệ thuộc lĩnh vực công nghệ thông tin và truyền thông phục vụ sự nghiệp công nghiệp hóa, hiện đại hóa đất nước, đáp ứng yêu cầu hội nhập quốc tế";
            string slogan = "Nâng tầm trí tuệ, Vươn Tới Tương Lai";
            string aboutContent = "{Slogan:'slogan...',Mission:'su menh...',Vision:'tam nhin...'}";
            aboutContent = Regex.Replace(aboutContent, "slogan...", slogan);
            aboutContent = Regex.Replace(aboutContent, "su menh...", mission);
            aboutContent = Regex.Replace(aboutContent, "tam nhin...", vision);



            context.Pages.AddOrUpdate(
                p => p.Alias,
                new Page { Title = "Contact", Content = "this is contact page", Author = "A", Alias = "contact" },
                new Page { Title = "About", Content = aboutContent, Author = "B", Alias = "about" },
                new Page { Title = "Home", Content = "{TinTucSuKien:'Lễ kỉ niệm 34 năm ngày Nhà giáo Việt Nam 20/11::Giải bóng đá Khoa Khoa học cơ bản mở rộng lần 2 năm 2016',KhoaHocCongNghe:'Lễ phát động Cuộc thi Sáng tạo KHCN ICTU lần I năm học 2016 – 2017::Khai giảng lớp Tập huấn lập trình 3D và thực tại ảo',TuyenDungViecLam:'Công Ty Cổ Phần Nam Việt Tuyển IT::Thông báo tuyển dụng công ty TNHH ITM semiconductor Vietnam'}", Author = "B", Alias = "home" },
                new Page {  Title = "What We Do", Content = "this is what we do page", Author = "C", Alias = "what-we-do" }
              );
            context.Slides.AddOrUpdate(
                p => p.ID,
                new Slide { Name="anh1",Url="Content/slides/pic_1.png",DisplayOrder=1,Description="some desc" }             ,
                new Slide {  Name = "anh2", Url = "Content/slides/pic_2.png", DisplayOrder = 2, Description = "some desc" },
                new Slide {  Name = "anh3", Url = "Content/slides/pic_3.png", DisplayOrder = 3, Description = "some desc" }
              );
            this.addAdminUser();
        }
        private bool addAdminUser()
        {
            bool success = false;

            var idManager = new IdentityManager();
            success = idManager.CreateRole("Admin", "Global Access");
            if (!success == true) return success;

            success = idManager.CreateRole("CanEdit", "Edit existing records");
            if (!success == true) return success;

            success = idManager.CreateRole("User", "Restricted to business domain activity");
            if (!success) return success;

            var newUser = new ApplicationUser()
            {
                UserName = "ADMIN",
                Email = "admin@example.com"
            };

            // Be careful here - you  will need to use a password which will
            // be valid under the password rules for the application,
            // or the process will abort:
            success = idManager.CreateUser(newUser, "123456@Az");
            if (!success) return success;

            success = idManager.AddUserToRole(newUser.Id, "Admin");
            if (!success) return success;

            success = idManager.AddUserToRole(newUser.Id, "CanEdit");
            if (!success) return success;

            success = idManager.AddUserToRole(newUser.Id, "User");
            if (!success) return success;

            return success;
        }
    }
}