namespace GEOTN.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addfeedback : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.Feedbacks");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Feedbacks",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Content = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
    }
}
