﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GEOTN.Model.Models
{
    [Table("Pages")]
    public class Page
    {
        [Key]
        [Required]
        [MaxLength(256)]
        [Column(TypeName = "varchar")]
        public string Alias { set; get; }

        [Required]
        [MaxLength(256)]
        public string Title { set; get; }

       

        [Display(Name = "Nội dung")]//, TODO: AllowHtml ! ]
        public string Content { set; get; }

        public string Author { set; get; }
    }
}