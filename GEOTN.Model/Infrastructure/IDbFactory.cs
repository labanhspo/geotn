﻿using System;

namespace GEOTN.Model.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        GEOTNDbContext Init();
    }
}