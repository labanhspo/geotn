﻿
namespace GEOTN.Model.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        private GEOTNDbContext dbContext;

        public GEOTNDbContext Init()
        {
            return dbContext ?? (dbContext = new GEOTNDbContext());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}