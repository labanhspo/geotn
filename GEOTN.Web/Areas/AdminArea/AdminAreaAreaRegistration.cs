﻿using System.Web.Mvc;

namespace GEOTN.Web.Areas.AdminArea
{
    [Authorize]
    public class AdminAreaAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "AdminArea";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "AdminArea_default",
                "Admin/{controller}/{action}/{id}",
                  defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "GEOTN.Web.Areas.AdminArea.Controllers" }

            );
        }
    }
}