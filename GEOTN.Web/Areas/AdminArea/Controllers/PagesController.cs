﻿using GEOTN.Model;
using GEOTN.Model.Models;
using GEOTN.Web.Areas.AdminArea.BaseClass;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.IO;
using System;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using System.Net.Http;
using System.Collections.Generic;

namespace GEOTN.Web.Areas.AdminArea.Controllers
{
    //  [Authorize(Roles ="Admin")]
    public class PagesController : AdminBaseController
    {
        private GEOTNDbContext db = new GEOTNDbContext();
        public static string templateUrl ="~/IntroApp/templates/";

        // GET: AdminArea/Pages
        public ActionResult Index()
        {
         
            return View(db.Pages.ToList());
        }

        // GET: AdminArea/Pages/Details/5
        public ActionResult Details(int? Alias)
        {
            if (Alias == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Page page = db.Pages.Find(Alias);
            if (page == null)
            {
                return HttpNotFound();
            }
            return View(page);
        }

        // GET: AdminArea/Pages/Create
        public ActionResult Create()
        {
          
            return View();
        }

        // POST: AdminArea/Pages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Title,Alias,Content,Author")] Page page)
        {
            if (ModelState.IsValid)
            {
                db.Pages.Add(page);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(page);
        }

        // GET: AdminArea/Pages/Edit/5
        public ActionResult Edit(int? Alias)
        {
            if (Alias == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Page page = db.Pages.Find(Alias);
            if (page == null)
            {
                return HttpNotFound();
            }
            return View(page);
        }

        // POST: AdminArea/Pages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Title,Alias,Content,Author")] Page page)
        {
            if (ModelState.IsValid)
            {
                db.Entry(page).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(page);
        }

        // GET: AdminArea/Pages/Delete/5
        public ActionResult Delete(int? Alias)
        {
            if (Alias == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Page page = db.Pages.Find(Alias);
            if (page == null)
            {
                return HttpNotFound();
            }
            return View(page);
        }

        // POST: AdminArea/Pages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int Alias)
        {
            Page page = db.Pages.Find(Alias);
            db.Pages.Remove(page);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        //public JsonResult GetThemeSupportedS(string name)
        //{ 
        //    if (name == null) Json(new { no="content" });
        //    var data = System.IO.File.ReadAllText(Server.MapPath(templateUrl+name));

        //    var regex = new Regex(@"\{{(.+?)\}}");
        //    MatchCollection matches = regex.Matches(data);            
        //    for (int i = 0; i < matches.Count; i++)
        //    {
        //        var temp = matches[i].ToString().Split('.')[1]; // {{content.info1}} =) info1}}
        //        var filterd = Regex.Replace(temp, @"\W+", "");  // }}
        //    }
        //    return Json(new { foo = "bar", baz = "Blech" }, JsonRequestBehavior.AllowGet);
        //}
        //public HttpResponseMessage GetThemeSupported()
        //{
        //    var data = db.Footers.ToList();
        //    if (data == null) return Request.CreateResponse(HttpStatusCode.NoContent);
        //    var json = "";
        //    IDictionary<string, string> tempData = new Dictionary<string, string>();
        //    for (int i = 0; i < data.Count; i++)
        //    {
        //        string key = data[i].ID;
        //        string value = data[i].Content;
        //        tempData.Add(key, value); // try catch Existed ID
        //    }

        //    json = JsonConvert.SerializeObject(tempData);

        //    return new HttpResponseMessage()
        //    {
        //        Content = new StringContent(json, Encoding.UTF8, "application/json")
        //        // Content = new StringContent(JArray.FromObject(data).ToString(), Encoding.UTF8, "application/json")
        //    };
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}