﻿using GEOTN.Web.Areas.AdminArea.BaseClass;
using System.Web.Mvc;

namespace GEOTN.Web.Areas.AdminArea.Controllers
{
    public class HomeController : AdminBaseController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}