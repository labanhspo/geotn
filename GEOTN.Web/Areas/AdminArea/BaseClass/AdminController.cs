﻿using System.Web.Mvc;

namespace GEOTN.Web.Areas.AdminArea.BaseClass
{
   [Authorize(Roles ="Admin")]
    public abstract class AdminBaseController : Controller
    {
    }
}