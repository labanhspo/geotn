﻿
$(function () {
    $('.textareaContent').froalaEditor({
        heightMin: 200,
        heightMax: 300
    })
});
var app = angular.module("MyApp");
app.controller('homeController', function ($scope, $http) {
    $scope.pageTypes = ["about", "what-we-do"];
    $scope.itemsInSelected = ["info1", "info2"];  // API read file template {{about.info1}} extracted info want to fill
    // write directive to render append  + -  Done  items   
    // end = stringify  items to  Page.Content = json string
});