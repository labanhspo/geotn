﻿var app = angular.module("IntroApp", ['ui.router']);
app.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');
    var homeState = {
        name: 'home',
        url: '/home',
        templateUrl: 'IntroApp/templates/home.html',
        controller: "homeController"
    }

    var aboutState = {
        name: 'about',
        url: '/about',
        templateUrl: 'IntroApp/templates/about.html',
        controller: 'aboutController'
    }

    var whatwedoState = {
        name: 'what-we-do', //for ui-sref=what-we-do
        url: '/what-we-do',
        templateUrl: 'IntroApp/templates/about.html',       
    }

    $stateProvider.state(homeState);
    $stateProvider.state(aboutState);
    $stateProvider.state(whatwedoState);
});
app.controller('aboutController', function ($scope, $http) {    
    $http({
        method: 'GET',
        url: '/api/Page?alias=about'
    }).then(function successCallback(response) {
        if (response.status == 204)
            console.log("khong thay noi dung");
        else 
            $scope.about = response.data;
    }, function errorCallback(response) {
        console.log("co gi do sai sai"+response.statusText);
    });

});
app.controller('homeController', function ($scope, $http) {
    $http.get("/api/Page?alias=home").then(function (response) {
        if (response.status == 204) console.log("khong thay noi dung");
        else  $scope.home = response.data;        
     }, function (response) {
         //Second function handles error
         $scope.errorBox = "Something went wrong";
         console.log("co gi do sai sai" + response.statusText);
     });
    $http.get("/api/Slide").then(function (response) {
        if (response.status == 204) console.log("khong thay noi dung");
        else $scope.slides = response.data;
     }, function (response) {
         //Second function handles error
         $scope.errorBox = "Something went wrong";
         console.log("co gi do sai sai" + response.statusText);
     });

    $http.get("/api/Footer").then(function (response) {
        if (response.status == 204) console.log("khong thay noi dung");
        else $scope.footer = response.data;
    }, function (response) {
        //Second function handles error
        $scope.errorBox = "Something went wrong";
        console.log("co gi do sai sai" + response.statusText);
    });

});
