﻿using GEOTN.Model;
using System.Web.Http;
using System.Linq;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Text;
using System;

namespace GEOTN.Web.Api
{
    
    [RoutePrefix("api/Page")]    
    public class PageController : ApiController
    {
        private GEOTNDbContext db = GEOTNDbContext.Create();
        /// <summary>
        /// Looks up some data by alias.
        /// If alias not found, return StatusCode = 204 (NoContent)
        /// If content of alias page not vaild, return StatusCode = 204 (NoContent)
        /// </summary>
        /// <param name="alias">The alias of the page.</param>
        /// 
        public HttpResponseMessage Get(string alias)
        {
             var page = db.Pages.SingleOrDefault(n => n.Alias == alias);
            if (page == null) return  Request.CreateResponse(HttpStatusCode.NoContent); 
            string data = page.Content;  // {'info1':'thong tin 1','info2':'thong tin 2'}
            JToken json;
            try { json = JObject.Parse(data); }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            //var results = JsonConvert.DeserializeObject<dynamic>(json);
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");
            // Content = new EncryptedContent(new CompressedContent(new JsonContent(json)))
            return response;
        }

    }
}
//var myList = JsonConvert.DeserializeObject<List<ListItems>>(jsonString);