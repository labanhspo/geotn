﻿using GEOTN.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace GEOTN.Web.Api
{
    public class SlideController : ApiController
    {
        private GEOTNDbContext db = GEOTNDbContext.Create();

        public HttpResponseMessage Get()
        {
            var data = db.Slides.ToList();
            if (data == null) return Request.CreateResponse(HttpStatusCode.NoContent);
            return new HttpResponseMessage()
            {
                Content = new StringContent(JArray.FromObject(data).ToString(), Encoding.UTF8, "application/json")             
            };
        }
        // GET  /api/Footer?key=timeOpen
   
    }
}