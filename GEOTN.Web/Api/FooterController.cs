﻿using GEOTN.Model;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace GEOTN.Web.Api
{
    public class FooterController : ApiController
    {
        private GEOTNDbContext db = GEOTNDbContext.Create();

        public HttpResponseMessage Get()
        {
            var data = db.Footers.ToList();
            if (data == null) return Request.CreateResponse(HttpStatusCode.NoContent);
            var json = "";
            IDictionary<string, string> tempData = new Dictionary<string, string>();
            for (int i = 0; i < data.Count; i++)
            {
                string key = data[i].ID;
                string value = data[i].Content;
                tempData.Add(key, value); // try catch Existed ID
            }

            json = JsonConvert.SerializeObject(tempData);

            return new HttpResponseMessage()
            {
                Content = new StringContent(json, Encoding.UTF8, "application/json")
                // Content = new StringContent(JArray.FromObject(data).ToString(), Encoding.UTF8, "application/json")
            };
        }
        // GET  /api/Footer?key=timeOpen
        public HttpResponseMessage Get(string key)
        {
            var data = db.Footers.Where(n => n.ID==key).ToList();
            if (data == null) return Request.CreateResponse(HttpStatusCode.NoContent);
            var json = "";
            IDictionary<string, string> tempData = new Dictionary<string, string>();
            for (int i = 0; i < data.Count; i++)
            {
                string _key = data[i].ID;
                string _value = data[i].Content;
                tempData.Add(_key, _value); // try catch Existed ID
            }

            json = JsonConvert.SerializeObject(tempData);

            return new HttpResponseMessage()
            {
                Content = new StringContent(json, Encoding.UTF8, "application/json")
                // Content = new StringContent(JArray.FromObject(data).ToString(), Encoding.UTF8, "application/json")
            };
        }
    }
}